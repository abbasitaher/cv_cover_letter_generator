
\centering{\huge \textbf{Research Statement}\normalsize}

\vspace{8px}
\raggedright
\large \textbf{Summary}\normalsize

\justify
The broad goal of my research is to design and implement highly scalable algorithms that enable complex systems, particularly the ones that utilize Artificial Intelligence and Machine Learning, to operate autonomously, robustly, and inexpensively.  Part of my research agenda is to exploit the growing power of Machine Learning to enable building reliable performance models using cheap resources that provide reliability, speed, and interpretability, wherever possible. Furthermore, the areas that I'm conducting research on require minimal need for expensive tools and hardware: Most of the times, a single laptop should suffice.

\noindent
\large \textbf{Vision and Research Approach}\normalsize

My research can be seen as a chain of observation phases, followed by appropriate action ones: observing the current issues in a field of research, and, taking actions to deal with these issues. As a recent example, discovering how hard algorithmic robot design problems can get~\cite{ghasemlou16wafr,saberifar16rss} is an observation of the problems in this field that I made, and providing efficient models for such design criteria~\cite{ghasemlou16wafr,sabgha2018ijrr} were the first set of actions I took to tackle it. The actions are usually not limited to presenting new models: in this example the main challenge was that such design spaces are immense, non-trivial and complex. Therefore, exact and deterministic methods are doomed to fail. So I followed two main, consecutive approaches to address these issues. First, based on my knowledge of traditional search and learning methods, I utilized a simple, but powerful classic approach (decision tree learning ~\cite{ghas2018iros}) and based on a theorem I proved to be correct~\cite{ghas2018iros}, showed that this approach could produce promising results. Second, building upon these results, I narrowed down the focus of the applications to increase the overall efficiency: I incorporated some domain knowledge into the process~\cite{ghas2019iros}. The results show an even better improvement, both in terms of efficiency and applicability to real world problems. 

To realize my research goal, I apply a wide range of different methods, by building machine learning models, and sometimes classic mathematical ones, at its core. I insist on strong evaluations with real-world systems whenever possible, or at least by using simulators fed by real world data. To address the increasingly interdisciplinary nature of modern computer science fields, I collaborate not only with roboticists, but I also reach out to researchers in other areas of computer science. At the Department of Computer Science and Engineering at the \emph{Mississippi State University}, I see a great potential for cooperation with all faculty members, in particular with \emph{Allen Parrish, Cindy Bethel, Khoa Luu, Eric Hansen, and Shahram Rahimi} in the areas of AI, Data Science and Machine Learning, and also with \emph{Ioana Banicescu, Tanmay Bhowmik, Max Young, Edward Luke and Ed Swan} in the areas of Software Engineering and theoretical Computer Science.

\noindent
\large \textbf{Research Plan}\normalsize

I will continue conducting research on different areas of Computer Science and combine different approaches and methods to design, develop and implement advanced methodologies. My previous researches on multi-robot exploration, SLAM, Machine Learning, Machine Vision, Minimal Robot Design, Combinatorial Filters, Procrustean Graphs and many other fields had to address a variety of issues and deal with different problems and this has helped me to gain a good vision for the projects that I plan to work on in the upcoming years. To fulfill these projects, my previous record of published papers will help me in getting funding from a variety of agencies, including the NSF, DARPA, NASA, and the NIH,  and companies such as Google and Facebook, as I have contributed to 7 such grants during my PhD studies. There are a growing number of laboratories working on similar projects and I see a very productive future for these fields.\\
\vspace{-10px}



\noindent
\textbf{Designing low cost AI systems: }\normalsize
One of the greatest challenges in Artificial Intelligence and Robotics is the high expense of doing research on real platforms. When a research team has to expand its domain from single agent methods to multi-agent ones, overcoming these expenses becomes even more critical. As many have shown, directly or indirectly, a considerable number of the systems that are being used in different AI applications are equipped with far more tools than they need. In fact, in these applications, the assigned tasks are possible to be done with simpler and cheaper systems. This shows that more research is needed on questions like: what kind of AI system, with which capabilities, is needed for a given task? Unlike many other research projects, where the research is mostly hardware oriented, these meta-level research projects require a deep mathematical understanding and a detailed formal method of modeling as a basis. I have already taken the first steps in some of my papers by providing a basis for such modeling and analysis. The next step is to study expensive systems that are utilizing large numbers of tools with costly maintenance procedures. Using mathematical modeling frameworks, including the aforementioned one that I have designed, I will identify simplifications that such systems are able to tolerate. In addition, using mathematical models, the procedure will be designed in such a way as to provide a scientific proof for the minimum requirements that the model has identified. This will assure the companies and their system design engineers that, despite lower initial and maintenance costs, the system will work as planned.\\
\vspace{-10px}

\noindent
\textbf{Minimal Design in Nano Robotics: }\normalsize
Given recent advancements in nanotechnology, nano-robots are growing in prominence as many other robotic platforms have before. One of the areas in which such robots and algorithms to deploy them are highly needed, with no need for expensive instruments, is nano-medicine. In nano-medicine, teams of nano-robots are deployed to monitor inside body organs, deliver medicine to cancer cells, or even perform surgeries. There are several major challenges that these robotic systems must address. First of all, these robots, due to their extremely small sizes, have a very limited computation power and a limited set of sensors and actuators. In addition, these robots must do their jobs in a limited time frame due to their limited energy sources. They should also be organized very efficiently in order to perform item or drug delivery and return to a specific point to be collected from the body. My experience in minimal robot design, multi-robot exploration and SLAM helps me in designing an efficient plan for these robots to achieve their goals with the lowest possible overall cost. In addition, based on my previous research on combinatorial filters and procrustean graphs, I plan to build detailed models of available robots and analyze these models to see the boundaries of limitations in a detailed manner for each one of them. This helps the scientific community to develop a realistic understanding of the power of such systems and the pros and cons of utilizing them.\\
\vspace{-10px}

\noindent
\textbf{Deep Learning in Multi-Robot Systems: }\normalsize 
Within the last few years, many robotic researchers have founded their methodologies based on bio-inspired ideas. One such popular recent approach to addressing different problems in the field of computer science, including robotics, has been to use Deep Learning. Our brains work by using a network of neurons to learn, memorize, imagine, and every other thing that we do. In my studies on multi-robot systems, I plan to extend the deep learning methods designed for single robots to multi-robot systems as well. Having several agents in comparison to one, in addition to giving the system more power from a hardware standpoint, also boosts the learning process of the resulting deep neural network. The more the data coming from different robots, the richer the learned network, and consequently the better the overall performance of the system. 
As another project in this area, I want to enhance the method that I  provided in a previous work on multi-robot exploration using a learning approach. The return pattern in my previous work was a static one. Using a learning method, this pattern would be able to dynamically adapt itself by comparing the outcomes of different attempts and adjust the return pattern accordingly. This enhancement makes the system even more uniform and less costly.\\

\noindent
\large \textbf{Funding}\normalsize

For the purpose of future funding, my research at the intersection of Algorithmic Robot Design, Multi-robot Exploration, Machine Learning, Machine Vision, and Algorithms would be of great interest to government funding agencies like DARPA and NSF, As I have contributed to several fundings from these agencies throughout my career, as well as tech companies interested in these areas, including, but not limited to Google, Facebook, Amazon, Tesla, Uber and Lyft.\\




